Summary
=======

DUMUX pub module for the Master Thesis of Maral Mohajer


Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installMohajer2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Mohajer2017a/raw/master/installMohajer2017a.sh)
in this folder.

```bash
mkdir -p Mohajer2017a && cd Mohajer2017a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Mohajer2017a/raw/master/installMohajer2017a.sh
sh ./installMohajer2017a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/aumin/,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/aumin/aumin.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.



Used Versions and Software
==========================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.

- DUNE 2.5 (common, grid, istl, geometry, localfunctions)
- dumux-stable 2.11


Results
=======

The final vtus for the simulated scenarios can be found in the /results folder.

All results discussed in Chapter 4 of the thesis are assuming zero background concentrations of Au species.

S1 : no effect of clay and organic matter on the precipitation of Au complexes or the adsorption of Au nanoparticles

S2: no effect of clay and organic matter on the precipitation of Au complexes, both affect the adsorption of Au nanoparticles

S3: clay and organic matter affect both the precipitation of Au complexes and the adsorption of Au nanoparticles

Appendix:

S1 with initialAu: same as S1, but with non-zero background concentrations of Au species, estimated from the Bernstorf report.



Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Mohajer2017a
cd Mohajer2017a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Mohajer2017a/-/raw/master/docker_mohajer2017a.sh
```

Open the Docker Container
```bash
bash docker_mohajer2017a.sh open
```

After the script has run successfully, you may build the executable by running

```bash
cd Mohajer2017a/build-cmake
make build_tests
```

It is located in the build-cmake/appl/aumin folder and it can be executed with an input file by running

```bash
cd appl/aumin
./aumin aumin.input
```
