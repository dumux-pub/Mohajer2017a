/*
 * biosusp.hh
 *
 *  Created on: 20.04.2011
 *      Author: hommel
 */

/*!
 * \file
 *
 * \brief A class for the Biosusp fluid properties
 */
#ifndef DUMUX_BIOSUSP_HH
#define DUMUX_BIOSUSP_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Biosusp fluid properties
 */
template <class Scalar>
class Biosusp : public Component<Scalar, Biosusp<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Biosusp.
     */
    static const char *name()
    { return "Suspended_Biomass"; }

    /*!
     * \brief The mass in [kg] of one mole of Biosusp.
     */
    static Scalar molarMass()  // TODO what is the molar Mass of suspended biomass???
    { return 1; } // kg/mol
        //based on a cell mass of 2.5e-16, the molar mass of cells would be 1.5e8 kg/mol.

    /*!
     * \brief The diffusion Coefficient of Biosusp in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1.5e-9; } // TODO is there anywhere in literature a meaningful biomass or cell diffusion coefficient?
    //{ return 2e-9; }

};

} // end namespace

#endif

