
install(FILES
        au.hh
        biofilm.hh
        bioproduct.hh
        biosub.hh
        biosusp.hh
        citrate.hh
        component.hh
        cyanide.hh
        water.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/components)
