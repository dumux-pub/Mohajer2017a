/*
 * biosub.hh
 *
 *  Created on: 20.04.2011
 *      Author: hommel
 */

/*!
 * \file
 *
 * \brief A class for the BioSubstrate fluid properties
 */
#ifndef DUMUX_BIOSUB_HH
#define DUMUX_BIOSUB_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Biosubstrate fluid properties
 */
template <class Scalar>
class Biosub : public Component<Scalar, Biosub<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Biosub.
     */
    static const char *name()
    { return "Substrate"; }

    /*!
     * \brief The mass in [kg] of one mole of Biosub.
     */
    static Scalar molarMass()   //Molar mass of Glucose is assumed.
    { return 0.18016; } // kg/mol

    /*!
     * \brief The diffusion Coefficient of Biosub in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1.5e-9; }
   // { return 2e-9; }

        // Check the right value TODO

};

} // end namespace

#endif

