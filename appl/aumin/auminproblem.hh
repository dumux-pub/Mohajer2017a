
// $Id: waterairproblem.hh 4185 2010-08-26 15:49:58Z lauser $
/*****************************************************************************
 *   Copyright (C) 2009 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License. or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_AUMINPROBLEM_HH
#define DUMUX_AUMINPROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dumux/material/fluidsystems/auminfluidsystem.hh>  // indexes come from hear
#include <dumux/porousmediumflow/implicit/problem.hh>
// #include <dumux/porousmediumflow/2pncmin/implicit/model.hh>
#include "model.hh"
#include "volumevariables.hh"

#include <dumux/material/binarycoefficients/brine_air.hh>
// #include <dumux/material/chemistry/biogeochemistry/auminreactions.hh>   // Microbes and some equations are hear

#include "auminspatialparams.hh"

#include "dumux/linear/seqsolverbackend.hh"


#define ISOTHERMAL 1

/*!
 * \ingroup BoxProblems
 * \brief TwoPNCMinBoxProblems  two-phase n-component mineralisation box problems
 */

namespace Dumux
{
template <class TypeTag>
class AuMinProblem;

namespace Properties
{
NEW_TYPE_TAG(AuMinProblem, INHERITS_FROM(BoxModel, TwoPNCMin, AuMinSpatialParams));

SET_TYPE_PROP(AuMinProblem, Model, TwoPAuMinModel<TypeTag>);

// Set the grid type
SET_PROP(AuMinProblem, Grid)
{
  typedef Dune::YaspGrid<2> type; // It is easier, I want to use rectangle so it is enough for me
};

// Set the problem property
SET_PROP(AuMinProblem, Problem)
{
    typedef Dumux::AuMinProblem<TTAG(AuMinProblem)> type;
};

SET_PROP(AuMinProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2O_Tabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::AuMinFluid<TypeTag, Scalar, H2O_Tabulated> type;

};

SET_TYPE_PROP(AuMinProblem, VolumeVariables, TwoPAuMinVolumeVariables<TypeTag>);

// SET_PROP(AuMinProblem, Chemistry)
// {
//     typedef Dumux::AuMinReactions<TypeTag> type;
// };

// Set the spatial parameters
SET_TYPE_PROP(AuMinProblem,
              SpatialParams,
              Dumux::AuMinSpatialParams<TypeTag>);

#if HAVE_UMFPACK
SET_TYPE_PROP(AuMinProblem, LinearSolver, UMFPackBackend<TypeTag>);
#endif

}

/*!
 * \ingroup TwoPTwoCNIBoxProblems
 * \brief Measurement of Brine displacement due to CO2 injection
 *  */
template <class TypeTag = TTAG(AuMinProblem) >
class AuMinProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GridView::Grid Grid;

//     typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef AuMinProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numComponents = FluidSystem::numComponents,

        numSPhases = FluidSystem::numSPhases,

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx, //  switch Index    in nonwetting phase.// CO2-phase saturation is used as primery variable instead of the mole fraction of total inorganic carbon in water ,
                                                   // whenever both fluid phases are present within the same control volum.

                                            // Primary variable Index of the equations(corespond to mass balance equation) and there is relation between these indexes and switch Index with M.B equation.

        xlBiosubIdx = FluidSystem::BiosubIdx,    // BioSubstrate (Food)
        xlnanoAuIdx = FluidSystem::nanoAuIdx,
        xlCNIdx = FluidSystem::CNIdx,
        xlCitrateIdx = FluidSystem::CitrateIdx,
        xlAuIIdx = FluidSystem::AuIIdx,
//         phiAuSolidIdx = numComponents,
//         phiImmNanoAuIdx = numComponents + 1,
//         phiDissBiofilmIdx = numComponents +2,//FluidSystem::dissBiofilmIdx,
//         phiPrecBiofilmIdx = numComponents +3,//FluidSystem::precBiofilmIdx,
        phiAuSolidIdx = FluidSystem::AuSolidIdx,
        phiImmNanoAuIdx = FluidSystem::immNanoAuIdx,
        phiDissBiofilmIdx = FluidSystem::dissBiofilmIdx,
        phiPrecBiofilmIdx = FluidSystem::precBiofilmIdx,

// #if !ISOTHERMAL   //                                 I think I don't need this part
//         temperatureIdx = Indices::temperatureIdx,
//         energyEqIdx = Indices::energyEqIdx,
// #endif

        //Indices of the components
        wCompIdx = FluidSystem::wCompIdx,     // wetting component Index
        nCompIdx = FluidSystem::nCompIdx,     // nonwetting component Index
        BiosubIdx = FluidSystem::BiosubIdx,
        AuIIdx = FluidSystem::AuIIdx,
        CNIdx = FluidSystem::CNIdx,
        CitrateIdx = FluidSystem::CitrateIdx,
        BrineIdx = FluidSystem::BrineIdx,
        AuSolidIdx = FluidSystem::AuSolidIdx,
        nanoAuIdx = FluidSystem::nanoAuIdx,
        immNanoAuIdx = FluidSystem::immNanoAuIdx,
        dissBiofilmIdx = FluidSystem::dissBiofilmIdx,
        precBiofilmIdx = FluidSystem::precBiofilmIdx,




        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        auPhaseIdx = FluidSystem::auPhaseIdx, // index of solid Au
        nanoAuPhaseIdx = FluidSystem::nanoAuPhaseIdx, // index of immobile nanoAu
        dissPhaseIdx = FluidSystem::dPhaseIdx, // index of the dissolving biofilm
        precPhaseIdx = FluidSystem::pPhaseIdx, // index of precipitating biofilm

        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;


public:
    AuMinProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        Dune::FMatrixPrecision<>::set_singular_limit(1e-35);

        try
    {
            name_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);

            outputEveryXTimeSteps_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, TimeManager, outputEveryXTimeSteps);

            //initial values
            initWaterPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initWaterPressure);  // initial pressure of water

            initSatAir_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initSatAir);
            initxlAuI_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlAuI);  // initial Primary variable Index
            initxlBiosub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlSubstrate);
            initxlCN_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCN);
            initxlCitrate_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCitrate);
            initxlnanoAu_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlnanoAu);


            initDissBiofilm_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initdBiofilm);
            initPrecBiofilm_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initpBiofilm);
            initAuSolid_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initAuSolid);
            initImmNanoAu_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initimmNanoAu);

            wFlux_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, waterFlux);
            rainFall_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, rainFall);

            injAuI_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injAuI);
            injBiosub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injBiosub);
            injCN_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCN);
            injCitrate_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCitrate);
            injnanoAu_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injnanoAu);

//         CCN_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, CCN);          //Concentration of Cyanide in wetting phase
//         CCitrate_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, Ccitrate);//concentration of Citrate in wetting phase
        cMn_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, cMn);         // concentration of Manganese



        Anano_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, Anano);   //mean specific surface Area of Au nano particles
        Alarge_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, Alarge);   //mean pecific surface Area of large Au particles

//         CAucomplex_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, CAucomplex);       //Concentration of Au complex(I,III) in wetting phase
//         CmaxAucomplex_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, CmaxAucomplex); //maximum Concentration of Au complex(I,III) in wetting phase
//         CminAucomplex_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, CminAucomplex); //minimum concentration of Au(I,III) in wetting phase

        kdiss_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kdiss);//dissolution rate coefficient
        kprec_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kprec);//precipitation rate coefficient
        kprecOM_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kprecOM);//precipitation rate coefficient
        kprecClay_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kprecClay);//precipitation rate coefficient
        OMContent_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, OMContent);//precipitation rate coefficient
        clayContent_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, ClayContent);//precipitation rate coefficient
        rhoSoil_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, rhoSoil);// attachment rate coefficient

//         ndiss_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients,ndiss); //
//         nprec_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients,nprec); //

        kdNano_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kdNano);// detachment rate coefficient
        kaNano_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kaNano);// attachment rate coefficient
        kaOM_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kaOM);// attachment rate coefficient
        kaClay_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, kaClay);// attachment rate coefficient

        RCNAu_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, RCNAu);// ratio of CN to Au (complexes)
        RCitrateAu_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, RCitrateAu);// ratio of Citrate to Au (nano particles)
        Rexcess_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, AuCoefficients, Rexcess);// defining the excess of CN and Citrate necessary due to other, competing metals


        mu_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients,mu);// maximum growth rate coefficient
        Ksub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients,Ksub);// Monod half saturation coefficient
        kd_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients,kd);// decay rate coefficient of bacteria
        cdAu_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients,cdAu);// decay rate coefficient of bacteria due to Au complexes
        cAuComplexCrit_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients,cAuComplexCrit);// threshold concentration above which bacteria start decaying due to Au complexes
        kAgg_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients,KAgg); // coefficient rate of aggregation

        BiomassYield_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, BiomassYield);// biomass Yield [kg/kg]
        CNYield_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, CNYield);// Yield of CN from substrate [mol/mol]
        CitrateYield_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, CitrateYield);// Yield of Citrate from substrate [mol/mol]
    }

    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }

//         FluidSystem::init();
        FluidSystem::init(/*startTemp=*/280.15, /*endTemp=*/282.15, /*tempSteps=*/3,
             /*startPressure=*/1e4, /*endPressure=*/5e6, /*pressureSteps=*/500);
        //this->timeManager().startNextEpisode(EpisodeEnd(0));
    }

    bool shouldWriteOutput() const
        {
            return
            this->timeManager().timeStepIndex() % outputEveryXTimeSteps_ == 0|| //output every X timesteps
            this->timeManager().willBeFinished();
               //true;
        }

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    void preTimeStep()
    {
    }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    //const char *name() const
    const std::string name() const
    {
        return name_;
    }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     */
    Scalar temperature(const Element &element,
                       const FVElementGeometry &fvGeometry,
                       int scvIdx) const
    {
        return 273.15 + 8; // -> 8 deg C
    };

    Scalar temperature() const
    {
        return 273.15 + 8; // -> 8 deg C
    };
#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition & globalPos = vertex.geometry().center();
        Scalar xmax = this->bBoxMax()[0];
        Scalar ymax = this->bBoxMax()[1];

        values.setAllNeumann();

//         if (globalPos[1] > ymax-eps_)
//         {
//             values.setDirichlet();
//             values.setDirichlet();
//             values.setDirichlet();
//         }

        if (globalPos[0] > xmax-eps_ || globalPos[0] < eps_ )
        {
//             values.setDirichlet(pressureIdx);
            values.setAllDirichlet();
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();

        initial_(values,globalPos);

        values[xlBiosubIdx] = 0;
        values[phiDissBiofilmIdx] = 0.0; // [m^3/m^3]
        values[phiPrecBiofilmIdx] = 0.0; // [m^3/m^3]
        values[phiAuSolidIdx] = 0.0; //initAuSolid_; // [m^3/m^3]
        values[phiImmNanoAuIdx] = 0.0; // [m^3/m^3]
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Element &element,
        const FVElementGeometry &fvGeometry,
        unsigned int &scvIdx) const
    {
//         return wPhaseOnly;
           return bothPhases;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void solDependentNeumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &is,
                      const int scvIdx,
                      const int boundaryFaceIdx,
                      const ElementVolumeVariables &volVars) const
    {
        const GlobalPosition &globalPos =fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
        Scalar ymax = this->bBoxMax()[1];

        values = 0.0;

        if(globalPos[1] > ymax -eps_)
        {
            values[pressureIdx] = -rainFall_*1000/FluidSystem::molarMass(pressureIdx);
            values[xlBiosubIdx] = -rainFall_* injBiosub_/FluidSystem::molarMass(BiosubIdx);
        }

//         if(globalPos[0] < eps_)
//         {
//             values[pressureIdx] = -wFlux_*1000/FluidSystem::molarMass(pressureIdx);
//             values[switchIdx] = 0.0;
//             values[xlBiosubIdx] = -wFlux_* injBiosub_/FluidSystem::molarMass(BiosubIdx);
//             values[xlnanoAuIdx] = -wFlux_* injnanoAu_/FluidSystem::molarMass(nanoAuIdx);
//             values[xlCNIdx] = -wFlux_* injCN_/FluidSystem::molarMass(CNIdx);
//             values[xlCitrateIdx] = -wFlux_*injCitrate_/FluidSystem::molarMass(CitrateIdx);
//             values[xlAuIIdx] = -wFlux_* injAuI_/FluidSystem::molarMass(AuIIdx);
//         }

  }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &source,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const

    {
     //         //TODO: include reactions directly here


        //         Scalar dt = this->timeManager().timeStepSize();


      source = 0;


    //Define some Parameters for conveniece
    Scalar initialPorosity =   elemVolVars[scvIdx].initialPorosity();
    Scalar biomassPrec = elemVolVars[scvIdx].precipitateVolumeFraction(precPhaseIdx) * elemVolVars[scvIdx].density(precPhaseIdx);
    Scalar biomassDiss = elemVolVars[scvIdx].precipitateVolumeFraction(dissPhaseIdx) * elemVolVars[scvIdx].density(dissPhaseIdx);
    Scalar cSubstrate = elemVolVars[scvIdx].moleFraction(wPhaseIdx, BiosubIdx) * elemVolVars[scvIdx].molarDensity(wPhaseIdx) * FluidSystem::molarMass(BiosubIdx);

    Scalar cAuComplex = elemVolVars[scvIdx].moleFraction(wPhaseIdx, AuIIdx) * elemVolVars[scvIdx].molarDensity(wPhaseIdx);
    Scalar cAuNanoMob = elemVolVars[scvIdx].moleFraction(wPhaseIdx, nanoAuIdx) * elemVolVars[scvIdx].molarDensity(wPhaseIdx);
    Scalar cCitrate = elemVolVars[scvIdx].moleFraction(wPhaseIdx, CitrateIdx) * elemVolVars[scvIdx].molarDensity(wPhaseIdx);
    Scalar cCN = elemVolVars[scvIdx].moleFraction(wPhaseIdx, CNIdx) * elemVolVars[scvIdx].molarDensity(wPhaseIdx);

    Scalar CAuNanoImmob = elemVolVars[scvIdx].precipitateVolumeFraction(nanoAuPhaseIdx) * elemVolVars[scvIdx].density(nanoAuPhaseIdx);
    Scalar phiAuNanoImmob = elemVolVars[scvIdx].precipitateVolumeFraction(nanoAuPhaseIdx);
    Scalar phiAuLarge = elemVolVars[scvIdx].precipitateVolumeFraction(auPhaseIdx);

//     std::cout<<" biomassPrec ="<<biomassPrec
//              <<"\n biomassDiss ="<<biomassDiss
//              <<"\n cAuComplex ="<<cAuComplex
//              <<"\n cAuNanoMob ="<<cAuNanoMob
//              <<"\n cCitrate ="<<cCitrate
//              <<"\n cCN ="<<cCN
//              <<"\n CAuNanoImmob ="<<CAuNanoImmob
//              <<"\n phiAuNanoImmob ="<<phiAuNanoImmob
//              <<"\n phiAuLarge ="<<phiAuLarge
//              <<"\n"<<std::endl;

//     //source terms for different gold components
//     Scalar qAularge = -rAulargediss_ + r aggregation_;
//     Scalar qAunnaomob = -ranano_ + rdnano_;
//     Scalar qAunanoimm = -rAunanoimmldiss_ + rprec_ + ranano_ -rdnano_ - raggregation_;
//     Scalar qAucomplex = rAulargediss_ + dissnanoimm_ - rprec_;

      //gold transformation
    // precipitation
      Scalar rprec = kprec_ * biomassPrec * cAuComplex;// mol/m³s
      rprec += kprecOM_ * OMContent_ * (1-initialPorosity) * rhoSoil_ * cAuComplex;// mol/m³s
      rprec += kprecClay_ * clayContent_ * (1-initialPorosity) * rhoSoil_ * cAuComplex;// mol/m³s


      //attachment-detachment (sorption)
      Scalar raNano = kaNano_ * cAuNanoMob; //mol/m³s
      raNano += kaOM_ * OMContent_ * (1-initialPorosity) * rhoSoil_ * cAuNanoMob;// mol/m³s
      raNano += kaClay_ * clayContent_ * (1-initialPorosity) * rhoSoil_ * cAuNanoMob;// mol/m³s
//       Scalar raNano = kaNano_ * (cAuNanoMob-cCitrate/(RCitrateAu_*Rexcess_)); //mol/m³s
//       raNano += kaOM_ * OMContent_ * (1-initialPorosity) * rhoSoil_ * (cAuNanoMob-cCitrate/(RCitrateAu_*Rexcess_));// mol/m³s
//       raNano += kaClay_ * clayContent_ * (1-initialPorosity) * rhoSoil_ * (cAuNanoMob-cCitrate/(RCitrateAu_*Rexcess_));// mol/m³s

      Scalar rdNano = kdNano_ * cCitrate * CAuNanoImmob; //mol/m³s

      //aggregation
      Scalar ragg = kAgg_ * CAuNanoImmob; //kg/m³s
      ragg /= FluidSystem::precipitateMolarMass(nanoAuPhaseIdx);

      //dissolution
      //Scalar const CMn;
      Scalar rAuNanoImmDiss = kdiss_ * cMn_ * Anano_ * phiAuNanoImmob * cCN;
      Scalar rAuLargeDiss =  kdiss_ * cMn_ * Alarge_ * phiAuLarge * cCN;

    //Microbial growth and decay
    Scalar rgPrec = mu_ * cSubstrate / (cSubstrate + Ksub_) * biomassPrec; //kg/m³s
    Scalar rgDiss = mu_ * cSubstrate / (cSubstrate + Ksub_) * biomassDiss; //kg/m³s

//     Scalar rdPrec = kd_ * biomassPrec;
//     Scalar rdDiss = kd_ * biomassDiss;

    Scalar rdPrec = (kd_ + cdAu_*(cAuComplex-cAuComplexCrit_))* biomassPrec;
    Scalar rdDiss = (kd_ + cdAu_*(cAuComplex-cAuComplexCrit_))* biomassDiss;

    //Nutrient consumption and Cyanide and Citrate production
    Scalar rSub = (rgPrec + rgDiss) /  BiomassYield_;  //kg/m³s
    rSub /= FluidSystem::molarMass(BiosubIdx); //rSub in mol/m³s
//     Scalar rProdCN = rgDiss /  BiomassYield_ * CNYield_; // mol/m³s
//     Scalar rProdCitrate = rgDiss /  BiomassYield_ * CitrateYield_; // mol/m³s
    Scalar rProdCN = rgDiss * CNYield_; // mol/m³s
    Scalar rProdCitrate = rgDiss * CitrateYield_; // mol/m³s

//         std::cout<<" rgPrec ="<<rgPrec
//              <<"\n rgDiss ="<<rgDiss
//              <<"\n rdPrec ="<<rdPrec
//              <<"\n rdDiss ="<<rdDiss
//              <<"\n rSub ="<<rSub
//              <<"\n rProdCN ="<<rProdCN
//              <<"\n rProdCitrate ="<<rProdCitrate
//              <<"\n rAuNanoImmDiss ="<<rAuNanoImmDiss
//              <<"\n rAuLargeDiss ="<<rAuLargeDiss
//              <<"\n rprec ="<<rprec
//              <<"\n rdNano ="<<rdNano
//              <<"\n raNano ="<<raNano
//              <<"\n ragg ="<<ragg
//              <<"\n "<<std::endl;

        //mol/m³s
        source[pressureIdx] = 0;
        source[switchIdx] = 0;
        source[xlAuIIdx] = rAuNanoImmDiss + rAuLargeDiss - rprec; //already in mol/m³s
        source[xlBiosubIdx] = -rSub; //already in mol/m³s
        source[xlCNIdx] = rProdCN - Rexcess_*RCNAu_*(rAuNanoImmDiss + rAuLargeDiss); //already in mol/m³s //actually, it is Au(CN)_2^-, a factor of 10 instead of 2 accounts for the "other" metal complexes consuming CN as well.
        source[xlCitrateIdx] =  rProdCitrate - rdNano * Rexcess_*RCitrateAu_;//already in mol/m³s
        source[xlnanoAuIdx] = rdNano - raNano;

        source[phiAuSolidIdx] = - rAuLargeDiss + ragg;
        source[phiImmNanoAuIdx] = rprec + raNano - rdNano - ragg - rAuNanoImmDiss; //already in mol/m³s
        source[phiDissBiofilmIdx] = (rgDiss - rdDiss) / FluidSystem::precipitateMolarMass(precPhaseIdx); //rg and rd in kg/m³s
        source[phiPrecBiofilmIdx] = (rgPrec - rdPrec) / FluidSystem::precipitateMolarMass(dissPhaseIdx); //rg and rd in kg/m³s

    }

//     /*!
//      * \brief Evaluate the source term for all phases within a given
//      *        sub-control-volume due to chemical reactions.
//      *
//      * For this method, the \a values parameter stores the rate mass
//      * of a component is generated or annihilate per volume
//      * unit. Positive values mean that mass is created, negative ones
//      * mean that it vanishes.
//      */
//     void reactionSource(PrimaryVariables &q,
//                 const Element &element,
//                 const FVElementGeometry &fvGeometry,
//                 int scvIdx,
//                 const VolumeVariables &volVars) const
//     {
//
//         q = 0;
//
//
//         Scalar dt = this->timeManager().timeStepSize();
//
// //         Chemistry chemistry;
// //         chemistry.reactionSource(q,
// //                         volVars,
// //                         dt);
//
//     }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */

//***In Aumin, initial amounts will be added later right ? as johannes answered the email. So what happens here , just let them as they are here or what to do ? or what are the possible cases , maybe try all of them //


    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        initial_(values, globalPos);
    }


private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        Scalar zmax = this->bBoxMax()[dimWorld-1];

        values[pressureIdx] = initWaterPressure_ + 1000*9.81*(zmax- globalPos[dimWorld-1]); //hydrostatic pressure
        values[switchIdx] = initSatAir_;
        values[xlAuIIdx] = initxlAuI_;
        values[xlBiosubIdx] = initxlBiosub_;
        values[xlCNIdx] = initxlCN_;
        values[xlCitrateIdx] = initxlCitrate_;
        values[xlnanoAuIdx] = initxlnanoAu_;

        values[phiDissBiofilmIdx] = initDissBiofilm_; // [m^3/m^3]
        values[phiPrecBiofilmIdx] = initPrecBiofilm_; // [m^3/m^3]
        values[phiAuSolidIdx] = 0.0; //initAuSolid_; // [m^3/m^3]
        values[phiImmNanoAuIdx] = initImmNanoAu_; // [m^3/m^3]

        if(inside(globalPos))
            values[phiAuSolidIdx] = initAuSolid_; // [m^3/m^3]
    }

    bool inside (const GlobalPosition &globalPos) const
    {
        Scalar xmax = this->bBoxMax()[0];
        Scalar zmax = this->bBoxMax()[dimWorld-1];

        if(globalPos[0]>xmax/2-0.1-eps_ && globalPos[0]<xmax/2+0.1+eps_
            && globalPos[1]>zmax-0.2-eps_ && globalPos[1]<zmax-0.1+eps_)
            return true;
        else
            return false;
    }

//    static const
//    Scalar EpisodeEnd (int episodeIdx)
//    {
//    }

    static constexpr Scalar eps_ = 1e-6;

    Scalar initWaterPressure_;
    Scalar initSatAir_;//2.3864e-7;       // [mol/mol]
    Scalar initxlAuI_;//0;
    Scalar initxlBiosub_;//2.97638e-4;
    Scalar initxlCN_;//0;
    Scalar initxlCitrate_;//
    Scalar initxlnanoAu_;//0;

    Scalar initDissBiofilm_;
    Scalar initPrecBiofilm_;
    Scalar initAuSolid_;
    Scalar initImmNanoAu_;

    Scalar injAuI_;
    Scalar injBiosub_;
    Scalar injCN_;
    Scalar injCitrate_;
    Scalar injnanoAu_;
    Scalar wFlux_;
    Scalar rainFall_;


//     Scalar CCN_ ;    //Concentration of Cyanide in wetting phase
//     Scalar CCitrate_ ; //concentration of Citrate in wetting phase
    Scalar cMn_ ;   // concentration of Manganese

    Scalar CAucomplex_ ;       //Concentration of Au complex(I,III) in wetting phase
    Scalar CmaxAucomplex_ ; //maximum Concentration of Au complex(I,III) in wetting phase
    Scalar CminAucomplex_ ; //minimum concentration of Au(I,III) in wetting phase

    Scalar kdiss_ ; //dissolution coefficient
    Scalar kprec_ ; //precipitation coefficient
    Scalar kprecOM_ ; //precipitation coefficient
    Scalar kprecClay_ ; //precipitation coefficient

    Scalar OMContent_;
    Scalar clayContent_;
    Scalar rhoSoil_;

    Scalar Anano_ ;  //Area
    Scalar Alarge_ ;  //Area

    Scalar rCNprod_ ; //Cyanide production rate
    Scalar rCitrateprod_ ; //Citrate production rate


    Scalar rdiss_ ;//dissolution rate
    Scalar rprec_ ;//precipitation rate


    Scalar rAulargediss_ ; // rate of dissolution of Au large
    Scalar rAunanoimmdiss_ ; //rate of dissolution of Au immobile nanoparticles
    Scalar ragg_ ; // rate of aggregation

    Scalar kdNano_ ;// detachment coefficient rate
    Scalar kaNano_ ;// attachment coefficient rate
    Scalar kaOM_ ;// attachment coefficient rate
    Scalar kaClay_ ;// attachment coefficient rate
    Scalar kAgg_ ;// aggregation coefficient rate

    Scalar mu_ ;// maxximum growth rate coefficient
    Scalar Ksub_ ;// maximum growth rate coefficient
    Scalar kd_ ;// decay rate of bacteria
    Scalar cdAu_;
    Scalar cAuComplexCrit_;

    Scalar BiomassYield_; //kg_substrate consumed/kg_biomass
    Scalar CNYield_; //mol CN/mol_substrate consumed
    Scalar CitrateYield_; //mol Citrate/mol_substrate consumed

    Scalar RCNAu_;
    Scalar RCitrateAu_;
    Scalar Rexcess_;

    std::string name_;
    int outputEveryXTimeSteps_;

};
} //end namespace

#endif
