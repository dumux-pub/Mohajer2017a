// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase, n-component mineralization model.
 */
#ifndef DUMUX_2PAUMIN_VOLUME_VARIABLES_HH
#define DUMUX_2PAUMIN_VOLUME_VARIABLES_HH

#include <vector>
#include <iostream>

#include <dumux/common/math.hh>
#include <dumux/implicit/model.hh>
#include <dumux/material/fluidstates/compositional.hh>
// #include <dumux/material/constraintsolvers/computefromreferencephase2pnc.hh>
// #include <dumux/material/constraintsolvers/miscible2pnccomposition.hh>
#include <dumux/porousmediumflow/2pncmin/implicit/volumevariables.hh>

#include <dumux/porousmediumflow/2pncmin/implicit/properties.hh>
#include <dumux/porousmediumflow/2pncmin/implicit/indices.hh>

namespace Dumux
{

/*!
 * \ingroup TwoPAuMinModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, n-component model.
 */
template <class TypeTag>
class TwoPAuMinVolumeVariables : public TwoPNCMinVolumeVariables<TypeTag>
{
    typedef TwoPNCMinVolumeVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum
    {
        dim = GridView::dimension,
        dimWorld=GridView::dimensionworld,

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numMajorComponents = GET_PROP_VALUE(TypeTag, NumMajorComponents),

        // formulations
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        pwsn = TwoPNCFormulation::pwsn,
        pnsw = TwoPNCFormulation::pnsw,

        // phase indices
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        // component indices
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        // phase presence enums
        nPhaseOnly = Indices::nPhaseOnly,
        wPhaseOnly = Indices::wPhaseOnly,
        bothPhases = Indices::bothPhases,

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        useSalinity = GET_PROP_VALUE(TypeTag, useSalinity)
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename Grid::ctype CoordScalar;
//     typedef Dumux::Miscible2pNCComposition<Scalar, FluidSystem> Miscible2pNCComposition;
//     typedef Dumux::ComputeFromReferencePhase2pNC<Scalar, FluidSystem> ComputeFromReferencePhase2pNC;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };
public:

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    /*!
     * \copydoc ImplicitVolumeVariables::update
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(priVars, problem, element, fvGeometry, scvIdx, isOldSol);
        completeFluidState(priVars, problem, element, fvGeometry, scvIdx, this->fluidState_, isOldSol);

//         /////////////
//         // calculate the remaining quantities
//         /////////////
//
//         // porosity evaluation
//         initialPorosity_ = problem.spatialParams().porosity(element, fvGeometry, scvIdx);
//         minimumPorosity_ = problem.spatialParams().porosityMin(element, fvGeometry, scvIdx);
//
//
//         sumPrecipitates_ = 0.0;
//         for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
//         {
//            precipitateVolumeFraction_[sPhaseIdx] = priVars[numComponents + sPhaseIdx];
//            sumPrecipitates_+= precipitateVolumeFraction_[sPhaseIdx];
//         }
//
//         // for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
//         // {
//         //     Chemistry chemistry; // the non static functions can not be called without abject
//         //     saturationIdx_[sPhaseIdx] = chemistry.omega(sPhaseIdx);
//         // }
//         // TODO/FIXME: The salt crust porosity is not clearly defined. However form literature review it is
//         // found that the salt crust have porosity of approx. 10 %. Thus we restrict the decrease in porosity
//         // to this limit. Moreover in the Problem files the precipitation should also be made dependent on local
//         // porosity value, as the porous media media properties change related to salt precipitation will not be
//         // accounted otherwise.
//
//         // this->porosity_ = initialPorosity_ - sumPrecipitates_;
//
//         this->porosity_ = std::max(minimumPorosity_, std::max(0.0, initialPorosity_ - sumPrecipitates_));
//
//         salinity_= 0.0;
//         moleFractionSalinity_ = 0.0;
//         for (int compIdx = numMajorComponents; compIdx< numComponents; compIdx++)    //sum of the mass fraction of the components
//         {
//             if(this->fluidState_.moleFraction(wPhaseIdx, compIdx)> 0)
//             {
//                 salinity_+= this->fluidState_.massFraction(wPhaseIdx, compIdx);
//                 moleFractionSalinity_ += this->fluidState_.moleFraction(wPhaseIdx, compIdx);
//             }
//         }
//
//         // TODO/FIXME: Different relations for the porosoty-permeability changes are given here. We have to fins a way
//         // so that one can select the relation form the input file.
//
//         // kozeny-Carman relation
//         permeabilityFactor_  =  std::pow(((1-initialPorosity_)/(1-this->porosity_)), 2)
//                                 * std::pow((this->porosity_/initialPorosity_), 3);
//
//         // Verma-Pruess relation
//         // permeabilityFactor_  =  100 * std::pow(((this->porosity_/initialPorosity_)-0.9),2);
//
//         // Modified Fair-Hatch relation with final porosity set to 0.2 and E1=1
//         // permeabilityFactor_  =  std::pow((this->porosity_/initialPorosity_),3)
//         //                         * std::pow((std::pow((1 - initialPorosity_),2/3))+(std::pow((0.2 - initialPorosity_),2/3)),2)
//         //                         / std::pow((std::pow((1 -this->porosity_),2/3))+(std::pow((0.2 -this->porosity_),2/3)),2);
//
//         //Timur relation with residual water saturation set to 0.001
//         // permeabilityFactor_ =  0.136 * (std::pow(this->porosity_,4.4)) / (2000 * (std::pow(0.001,2)));
//
//         //Timur relation1 with residual water saturation set to 0.001
//         // permeabilityFactor_ =  0.136 * (std::pow(this->porosity_,4.4)) / (200000 * (std::pow(0.001,2)));
//
//         // Bern. relation
//         // permeabilityFactor_ = std::pow((this->porosity_/initialPorosity_),8);
//
//         //Tixier relation with residual water saturation set to 0.001
//         // permeabilityFactor_ = (std::pow((250 * (std::pow(this->porosity_,3)) / 0.001),2)) / initialPermeability_;
//
//         //Coates relation with residual water saturation set to 0.001
//         // permeabilityFactor_ = (std::pow((100 * (std::pow(this->porosity_,2)) * (1-0.001) / 0.001,2))) / initialPermeability_ ;
//
//         // energy related quantities not contained in the fluid state
//         //asImp_().updateEnergy_(priVars, problem,element, fvGeometry, scvIdx, isOldSol);
    }

   /*!
    * \copydoc ImplicitModel::completeFluidState
    * \param isOldSol Specifies whether this is the previous solution or the current one
    */
    static void completeFluidState(const PrimaryVariables& priVars,
                                   const Problem& problem,
                                   const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   int scvIdx,
                                   FluidState& fluidState,
                                   bool isOldSol = false)

    {
        Scalar t = Implementation::temperature_(priVars, problem, element,fvGeometry, scvIdx);
        fluidState.setTemperature(t);

        int dofIdxGlobal = problem.model().dofMapper().subIndex(element, scvIdx, dofCodim);
        int phasePresence = problem.model().phasePresence(dofIdxGlobal, isOldSol);

        /////////////
        // set the saturations
        /////////////

        Scalar Sg;
        if (phasePresence == nPhaseOnly)
        {
            Sg = 1.0;
        }
        else if (phasePresence == wPhaseOnly)
        {
            Sg = 0.0;
        }
        else if (phasePresence == bothPhases)
        {
            if (formulation == pwsn)
                Sg = priVars[switchIdx];
            else if (formulation == pnsw)
                Sg = 1.0 - priVars[switchIdx];
            else
                DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "phasePresence: " << phasePresence << " is invalid.");
        fluidState.setSaturation(nPhaseIdx, Sg);
        fluidState.setSaturation(wPhaseIdx, 1.0 - Sg);

        /////////////
        // set the pressures of the fluid phases
        /////////////

        // calculate capillary pressure
        const MaterialLawParams &materialParams = problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);
        Scalar pc = MaterialLaw::pc(materialParams, 1 - Sg);

        // extract the pressures
        if (formulation == pwsn) {
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx]);
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx] + pc);
        }
        else if (formulation == pnsw) {
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx]);
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx] - pc);
        }
        else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");

        /////////////
        // calculate the phase compositions
        /////////////

        typename FluidSystem::ParameterCache paramCache;

        // now comes the tricky part: calculate phase composition
        if (phasePresence == bothPhases)
        {
            // both phases are present, phase composition results from
            // the gas <-> liquid equilibrium. This is
            // the job of the "MiscibleMultiPhaseComposition"
            // constraint solver

            Scalar moleFractionNotWater = 0.0;
            // set the known mole fractions (all except water and air)
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                fluidState.setMoleFraction(wPhaseIdx, compIdx, priVars[compIdx]);
                moleFractionNotWater += priVars[compIdx];
                fluidState.setMoleFraction(nPhaseIdx, compIdx, 0.0);
            }

        // calulate the equilibrium composition for the given
        // temperature and pressure.
        Scalar temperature = fluidState.temperature();
        Scalar pg = fluidState.pressure(nPhaseIdx);

        //calculate the amount of air in water:
        Scalar A = computeA_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar B = computeB_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar yH2OinGas = (1 - B) / (1. / A - B); // equilibrium mol fraction of H2O in the gas phase
        Scalar xAirinWater = B * (1 - yH2OinGas); // equilibrium mol fraction of Air in the water phase

        //set mole fractions of water and air in the gas phase
        fluidState.setMoleFraction(nPhaseIdx, wCompIdx, yH2OinGas);
        fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-yH2OinGas);

        //set mole fractions in the liquid phase
        fluidState.setMoleFraction(wPhaseIdx, nCompIdx, xAirinWater);
        moleFractionNotWater += xAirinWater;
        fluidState.setMoleFraction(wPhaseIdx, wCompIdx, 1- moleFractionNotWater);
        }

        else if (phasePresence == nPhaseOnly)
        {
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                fluidState.setMoleFraction(wPhaseIdx, compIdx, 0.0);
                fluidState.setMoleFraction(nPhaseIdx, compIdx, 0.0);
            }

        Scalar temperature = fluidState.temperature();
        Scalar pg = fluidState.pressure(nPhaseIdx);

        //calculate the amount of air in water:
        Scalar A = computeA_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar B = computeB_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar yH2OinGas = (1 - B) / (1. / A - B); // equilibrium mol fraction of H2O in the gas phase
        Scalar xAirinWater = B * (1 - yH2OinGas); // equilibrium mol fraction of Air in the water phase

        //set mole fractions of water and air in the gas phase
        fluidState.setMoleFraction(nPhaseIdx, wCompIdx, yH2OinGas);
        fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-yH2OinGas);

        //set mole fractions in the liquid phase
        fluidState.setMoleFraction(wPhaseIdx, nCompIdx, xAirinWater);
        fluidState.setMoleFraction(wPhaseIdx, wCompIdx, 1- xAirinWater);

//             Dune::FieldVector<Scalar, numComponents> moleFrac;
//             Dune::FieldVector<Scalar, numComponents> fugCoeffL;
//             Dune::FieldVector<Scalar, numComponents> fugCoeffG;
//
//             for (int compIdx=0; compIdx<numComponents; ++compIdx)
//             {
//                 fugCoeffL[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
//                                         paramCache,
//                                         wPhaseIdx,
//                                         compIdx);
//                 fugCoeffG[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
//                                         paramCache,
//                                         nPhaseIdx,
//                                         compIdx);
//             }
//             for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
//                 moleFrac[compIdx] = (priVars[compIdx]*fugCoeffL[compIdx]*fluidState.pressure(wPhaseIdx))
//                 /(fugCoeffG[compIdx]*fluidState.pressure(nPhaseIdx));
//
//             moleFrac[wCompIdx] =  priVars[switchIdx];
//             Scalar sumMoleFracNotGas = 0;
//             for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
//             {
//                     sumMoleFracNotGas+=moleFrac[compIdx];
//             }
//             sumMoleFracNotGas += moleFrac[wCompIdx];
//             moleFrac[nCompIdx] = 1 - sumMoleFracNotGas;
//
//             // Set fluid state mole fractions
//             for (int compIdx=0; compIdx<numComponents; ++compIdx)
//             {
//                 fluidState.setMoleFraction(nPhaseIdx, compIdx, moleFrac[compIdx]);
//             }
//
//             // calculate the composition of the remaining phases (as
//             // well as the densities of all phases). this is the job
//             // of the "ComputeFromReferencePhase2pNc" constraint solver
//             ComputeFromReferencePhase2pNC::solve(fluidState,
//                                                     paramCache,
//                                                     nPhaseIdx,
//                                                     nPhaseOnly,
//                                                     /*setViscosity=*/true,
//                                                     /*setInternalEnergy=*/false);

            }
        else if (phasePresence == wPhaseOnly)
        {

            // only the liquid phase is present, i.e. liquid phase
            // composition is stored explicitly.
            // extract _mass_ fractions in the gas phase
            Scalar moleFractionNotWater = 0.0;
            // set the known mole fractions (all except water and air)
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                fluidState.setMoleFraction(wPhaseIdx, compIdx, priVars[compIdx]);
                moleFractionNotWater += priVars[compIdx];
                fluidState.setMoleFraction(nPhaseIdx, compIdx, 0.0);
            }

        // calulate the equilibrium composition for the given
        // temperature and pressure.
        Scalar temperature = fluidState.temperature();
        Scalar pg = fluidState.pressure(nPhaseIdx);

        //calculate the amount of air in water:
        Scalar A = computeA_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar B = computeB_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar yH2OinGas = (1 - B) / (1. / A - B); // equilibrium mol fraction of H2O in the gas phase

        //set mole fractions of water and air in the gas phase
        fluidState.setMoleFraction(nPhaseIdx, wCompIdx, yH2OinGas);
        fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-yH2OinGas);

        //set mole fractions in the liquid phase
        fluidState.setMoleFraction(wPhaseIdx, nCompIdx, priVars[nCompIdx]);
        moleFractionNotWater += priVars[nCompIdx];
        fluidState.setMoleFraction(wPhaseIdx, wCompIdx, 1- moleFractionNotWater);
        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }
    }
    /*!
     * \brief Returns the volume fraction of the precipitate (solid phase)
     * for the given phaseIdx
     *
     * \param phaseIdx the index of the solid phase
     */
    Scalar precipitateVolumeFraction(int phaseIdx) const
    {
        assert(numPhases <= phaseIdx && phaseIdx < numPhases+numSPhases);
        return ParentType::precipitateVolumeFraction_[phaseIdx - numPhases];
    }

//     /*!
//      * \brief Returns the inital porosity of the
//      * pure, precipitate-free porous medium
//      */
//     Scalar initialPorosity() const
//     { return initialPorosity_;}
//
//     /*!
//      * \brief Returns the inital permeability of the
//      * pure, precipitate-free porous medium
//      */
//     Scalar initialPermeability() const
//     { return initialPermeability_;}
//
    /*!
     * \brief Returns the factor for the reduction of the initial permeability
     * due precipitates in the porous medium
     */
    Scalar permeabilityFactor() const
    { return ParentType::permeabilityFactor_; }
//
//     /*!
//      * \brief Returns the mole fraction of the salinity in the liquid phase
//      */
//     Scalar moleFracSalinity() const
//     {
//         return moleFractionSalinity_;
//     }
//
//     /*!
//      * \brief Returns the salinity (mass fraction) in the liquid phase
//      */
//     Scalar salinity() const
//     {
//         return salinity_;
//     }
//
//     /*!
//      * \brief Returns the density of the phase for all fluid and solid phases
//      *
//      * \param phaseIdx the index of the fluid phase
//      */
//     Scalar density(int phaseIdx) const
//     {
//         if (phaseIdx < numPhases)
//             return this->fluidState_.density(phaseIdx);
//         else if (phaseIdx >= numPhases)
//             return FluidSystem::precipitateDensity(phaseIdx);
//         else
//             DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
//     }
//     /*!
//      * \brief Returns the mass density of a given phase within the
//      *        control volume.
//      *
//      * \param phaseIdx The phase index
//      */
//     Scalar molarDensity(int phaseIdx) const
//     {
//         if (phaseIdx < numPhases)
//             return this->fluidState_.molarDensity(phaseIdx);
//         else if (phaseIdx >= numPhases)
//             return FluidSystem::precipitateMolarDensity(phaseIdx);
//         else
//             DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
//     }
//
//     /*!
//      * \brief Returns the molality of a component in the phase
//      *
//      * \param phaseIdx the index of the fluid phase
//      * \param compIdx the index of the component
//      * \f$\mathrm{molality}=\frac{n_\mathrm{component}}{m_\mathrm{solvent}}
//      * =\frac{n_\mathrm{component}}{n_\mathrm{solvent}*M_\mathrm{solvent}}\f$
//      * compIdx of the main component (solvent) in the
//      * phase is equal to the phaseIdx
//      *//run/media/maral/workspace23/DumuxAuMingit/dumux-devel/appl/aumin/volumevariables.hh:124:27: error: cannot call member function ‘void Dumux::TwoPNCMinVolumeVariables<TypeTag>::update(const PrimaryVariables&, const Problem&, const Element&, const FVElementGeometry&, int, bool) [with TypeTag = Dumux::Properties::TTag::AuMinProblem; Dumux::TwoPNCMinVolumeVariables<TypeTag>::PrimaryVariables = Dune::FieldVector<double, 11>; Dumux::TwoPNCMinVolumeVariables<TypeTag>::Problem = Dumux::AuMinProblem<Dumux::Properties::TTag::AuMinProblem>; Dumux::TwoPNCMinVolumeVariables<TypeTag>::Element = Dune::Entity<0, 2, const Dune::YaspGrid<2>, Dune::YaspEntity>; Dumux::TwoPNCMinVolumeVariables<TypeTag>::FVElementGeometry = Dumux::BoxFVElementGeometry<Dumux::Properties::TTag::AuMinProblem>]’ without object
//
//     Scalar molality(int phaseIdx, int compIdx) const // [moles/Kg]
//     { return this->fluidState_.moleFraction(phaseIdx, compIdx)
//                   /(fluidState_.moleFraction(phaseIdx, phaseIdx)
//                   * FluidSystem::molarMass(phaseIdx));}

protected:
//     friend class TwoPNCVolumeVariables<TypeTag>;
//     static Scalar temperature_(const PrimaryVariables &priVars,
//                                 const Problem& problem,
//                                 const Element &element,
//                                 const FVElementGeometry &fvGeometry,
//                                 int scvIdx)
//     {
//         return problem.temperatureAtPos(fvGeometry.subContVol[scvIdx].global);
//     }
//
//     template<class ParameterCache>
//     static Scalar enthalpy_(const FluidState& fluidState,
//                             const ParameterCache& paramCache,
//                             int phaseIdx)
//     {
//         return 0;
//     }
//
//    /*!
//     * \brief Update all quantities for a given control volume.
//     *
//     * \param priVars The solution primary variables
//     * \param problem The problem
//     * \param element The element
//     * \param fvGeometry Evaluate function with solution of current or previous time step
//     * \param scvIdx The local index of the SCV (sub-control volume)
//     * \param isOldSol Evaluate function with solution of current or previous time step
//     */
//     void updateEnergy_(const PrimaryVariables &priVars,
//                        const Problem &problem,
//                        const Element &element,
//                        const FVElementGeometry &fvGeometry,
//                        const int scvIdx,
//                        bool isOldSol)
//     {};

    Scalar precipitateVolumeFraction_[numSPhases];
    Scalar permeabilityFactor_;
    Scalar initialPorosity_;
    Scalar initialPermeability_;
    Scalar minimumPorosity_;
    Scalar sumPrecipitates_;
    Scalar salinity_;
    Scalar moleFractionSalinity_;
    FluidState fluidState_;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

        /*!
    * \brief computation of mu_{Air}^{l(0)}/RT
    * \param T the temperature [K]
    * \param pg the gas phase pressure [Pa]
    */
    static Scalar computeA_(Scalar T, Scalar pg)
    {
        static const Scalar c[10] = {
            28.9447706,
            -0.0354581768,
            -4770.67077,
            1.02782768E-5,
            33.8126098,
            9.04037140E-3,
            -1.14934031E-3,
            -0.307405726,
            -0.0907301486,
            9.32713393E-4,
        };

        const Scalar pg_bar = pg / 1.0E5; /* conversion from Pa to bar */
        const Scalar Tr = 630.0 - T;

        return
            c[0] +
            c[1]*T +
            c[2]/T +
            c[3]*T*T +
            c[4]/Tr +
            c[5]*pg_bar +
            c[6]*pg_bar*log(T) +
            c[7]*pg_bar/T +
            c[8]*pg_bar/Tr +
            c[9]*pg_bar*pg_bar/(Tr*Tr);
    }

            /*!
     * \brief computation of B
     *
     * \param T the temperature [K]
     * \param pg the gas phase pressure [Pa]
     */
    static Scalar computeB_(Scalar T, Scalar pg)
    {
        const Scalar c1 = -0.411370585;
        const Scalar c2 = 6.07632013E-4;
        const Scalar c3 = 97.5347708;
        const Scalar c8 = -0.0237622469;
        const Scalar c9 = 0.0170656236;
        const Scalar c11 = 1.41335834E-5;

        const Scalar pg_bar = pg / 1.0E5; /* conversion from Pa to bar */

        return
            c1 +
            c2*T +
            c3/T +
            c8*pg_bar/T +
            c9*pg_bar/(630.0-T) +
            c11*T*std::log(pg_bar);
    }

        /*!
     * \brief Returns the equilibrium molality of Air \f$\mathrm{(mol Air / kg water)}\f$ for a
     * Air-water mixture at a given pressure and temperature
     *
     * \param T the temperature \f$\mathrm{[K]}\f$
     * \param pg the gas phase pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar molalityAirinPureWater_(Scalar temperature, Scalar pg) {
        Scalar A = computeA_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar B = computeB_(temperature, pg); // according to Spycher, Pruess and Ennis-King (2003)
        Scalar yH2OinGas = (1 - B) / (1. / A - B); // equilibrium mol fraction of H2O in the gas phase
        Scalar xAirinWater = B * (1 - yH2OinGas); // equilibrium mol fraction of Air in the water phase
        Scalar molalityAir = (xAirinWater * 55.508) / (1 - xAirinWater); // Air molality
        return molalityAir;
    }
};

} // end namespace

#endif
